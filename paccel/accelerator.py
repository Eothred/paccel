#!/usr/bin/python
from particle import particle
import yaml
import numpy as np
from elements import drift,dipole,quadrupole
##
# @file accelerator.py
# 
# This is a parent class that holds all information of
# an accelerator
#

class accelerator:
    def __init__(self,name='noname',nparticles=10):
        self.name=name
        self.sequence=[]
        self.length=0.
        self.particles=[particle(str(j)) for j in xrange(nparticles)]
    def __iter__(self):
        return iter(self.sequence)
    def addElement(self,element):
        self.sequence.append(element)
        self.length+=self.sequence[-1].getLength()
        return 0
    def printStrengthTable(self):
        for el in self:
            print(el.getName()+" ("+el.getType()+"): "+str(el.getStrength()).rjust(20))
    ##
    # Tracks the particles the number of turns around the accelerator..
    def trackParticles(self,turns,fout=''):
        if fout:
            f=file(fout,'w')
        for j in xrange(turns):
            for elem in self:
                elem.transport(self.particles,f)
    ##
    # Returns a dictionary with the sequence
    def getDictionary(self):
        ret={}
        for el in self:
            ret[el.getName()]={el.getType():el.getInputs()}
        return {self.name: ret}
    ##
    # Saves the sequence in YAML format
    def writeSequence(self,filename):
        fout=file(filename,'w')
        acc_dict=self.getDictionary()
        seq=[]
        for el in self:
           seq.append(el.getName())
        acc_dict['sequence']=seq
        fout.write(yaml.safe_dump(acc_dict))
    
    ##
    #
    def getTransferMatrix(self):
        ret=np.zeros((4,4))
        for i in xrange(4):
            ret[i,i]=1.
        for el in self:
            if el.isLinear:
                ret=ret.dot(el.matrix)
        return ret
    def loadFromFile(self,ymlfile):
        fin=file(ymlfile,'r')
        d=yaml.load(fin)
        fin.close()
        self.loadFromDict(d)
    def loadFromDict(self,dictionary):
        self.name=dictionary.keys()[0]
        for el in dictionary['sequence']:
            if dictionary[self.name][el].keys()[0]=='dipole.dipole':
                arr=dictionary[self.name][el]['dipole.dipole']
                self.addElement(dipole.dipole(arr[0],
                                       arr[1],
                                       arr[2],
                                       arr[3]))
            elif dictionary[self.name][el].keys()[0]=='dipole.edge':
                arr=dictionary[self.name][el]['dipole.edge']
                self.addElement(dipole.edge(arr[0],
                                       arr[1],
                                       arr[2],
                                       arr[3]))
            elif dictionary[self.name][el].keys()[0]=='quadrupole.quadrupole':
                arr=dictionary[self.name][el]['quadrupole.quadrupole']
                self.addElement(quadrupole.quadrupole(arr[0],
                                       arr[1],
                                       arr[2],
                                       arr[3]))
            elif dictionary[self.name][el].keys()[0]=='drift.drift':
                arr=dictionary[self.name][el]['drift.drift']
                self.addElement(drift.drift(arr[0],
                                       arr[1],
                                       arr[2]))
            else:
                print "Could not understand element type:",dictionary[self.name][el]
