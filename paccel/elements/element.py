#!/usr/bin/python

import numpy as np

##
# @file element.py
# @brief The base that all elements builds on.
# 
# @author Yngve Inntjore Levinsen
def _checkInput(name,length,pos):
    if type(name)!=str and type(name)!=unicode:
        print type(name)
        raise TypeError,'name must be a string'
    if type(length)!=int and type(length)!=float:
        raise TypeError
    if np.shape(pos) != (2,3):
        print pos, type(pos)
        raise ValueError, 'position should have shape (2,3)'
class element:
    def __init__(self,name,length,pos):
        _checkInput(name,length,pos)
        self.name=name
        self.length=length
        self.isLinear=False
        ## 
        # contains position and rotation of element
        #  in form
        # [  x, y, z ]
        # [ px,py,pz ]
        self.pos=pos[:]
        self.misalignment=np.array((2,3))
    
    def getLength(self):
        return self.length
    def getName(self):
        return self.name
    
    def transport(self,particles,fout=None):
        if fout!=None:
            if type(particles)==type([]):
                for p in particles:
                    self._transportNwrite(p,fout)
            else: # single particle
                self._transportNwrite(particles,fout)
        else:
            if type(particles)==type([]):
                for p in particles:
                    self._transport(p)
            else: # single particle
                self._transport(particles)
