#!/usr/bin/python
import numpy as np
from magnet import magnet

class drift(magnet):
    def __init__(self,name,length,pos):
        magnet.__init__(self,name,length,pos)
        self.matrix=np.zeros((4,4))
        
        # By default a drift section:
        self.matrix[0,0]=self.matrix[1,1]=1.
        self.matrix[2,2]=self.matrix[3,3]=1.
        self.matrix[0,1]=self.matrix[2,3]=self.length
        
        self.isLinear=True
        
        self.rj=20
    def getTransMatrix(self):
        return self.matrix[:]
        
    def _transport(self,particle):
        oldpos=particle.get4d()
        newpos=self.matrix.dot(oldpos.reshape(4,1)).reshape(2,2)
        particle.set4d(newpos,self.length)
        return newpos
    
    def _transportNwrite(self,particle,fout):
        ret=self._transport(particle)
        fout.write(particle.getID().rjust(self.rj/2))
        for axis in ret:
            for coord in axis:
                fout.write(str(coord).rjust(self.rj))
        fout.write(str(particle.getSpos()).rjust(self.rj))
        fout.write("\n")
        return ret
    def getInputs(self):
        return [self.name,self.length,self.pos.tolist()]
    def getType(self):
        return 'drift.drift'
