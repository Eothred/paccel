#!/usr/bin/python
import numpy as np
from drift import drift

##
# @file dipole.py
# 
# Dipole element is a basic element.
# It has magnetic poles up to ... order

class dipole(drift):
    def __init__(self,name,length,pos,R):
        drift.__init__(self,name,length,pos)
        kl=length/R
        # Horizontal:
        self.matrix[0,0]=np.cos(kl)
        self.matrix[0,1]=np.sin(kl)*R
        self.matrix[1,0]=-np.sin(kl)/R
        self.matrix[1,1]=np.cos(kl)
        
        # settings:
        self.R=R

    def getStrength(self):
        return self.R
    def getType(self):
        return 'dipole.dipole'
    def getMagnetError(self):
        print 'no error implemented'
    def getInputs(self):
        return [self.name,self.length,self.pos.tolist(),self.R]
        

class edge(drift):
    def __init__(self,name,pos,R,Psi):
        drift.__init__(self,name,0.,pos)
        self.matrix[1,0]=np.tan(Psi)/R
        self.matrix[3,2]=-np.tan(Psi)/R
        self.Psi=Psi
        self.R=R
    def getType(self):
        return 'dipole.edge'
    def getInputs(self):
        return [self.name,self.pos.tolist(),self.R,self.Psi]
        