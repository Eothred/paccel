
#!/usr/bin/python
import numpy as np
from paccel.elements.drift import drift
##
# @file quadrupole.py
# 

class quadrupole(drift):
    def __init__(self,name,length,pos,k):
        drift.__init__(self,name,length,pos)
        sqkl=length*np.sqrt(abs(k))
        sqk=np.sqrt(abs(k))
        self.k=k
        if k>0:
            fi,fj=0,1
            di,dj=2,3
        else:
            di,dj=0,1
            fi,fj=2,3

        # Focusing:
        self.matrix[fi,fi]=np.cos(sqkl)
        self.matrix[fi,fj]=np.sin(sqkl)/sqk
        self.matrix[fj,fi]=-np.sin(sqkl)*sqk
        self.matrix[fj,fj]=np.cos(sqkl)
        
        # Defocusing:
        self.matrix[di,di]=np.cosh(sqkl)
        self.matrix[di,dj]=np.sinh(sqkl)/sqk
        self.matrix[dj,di]=np.sinh(sqkl)*sqk
        self.matrix[dj,dj]=np.cosh(sqkl)
        
        # fix for 0 division:
        if sqk==0:
            self.matrix[fi,fj]=self.matrix[di,dj]=self.length
            
        
    
    def getStrength(self):
        return self.k
    def getType(self):
        return 'quadrupole.quadrupole'
    def getInputs(self):
        return [self.name,self.length,self.pos.tolist(),self.k]