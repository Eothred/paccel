import numpy as np

# Coordinates are:
# [[ x, xp, dp/p] [y,yp,E]]
# 
class particle():
    def __init__(self,ID):
        self._coord=np.random.randn(6).reshape(2,3)*.0001
        self._ID=ID
        self._s=0.
    def get4d(self):
        return self._coord[:2,:2]
    def get6d(self):
        return self._coord[:]
    def set4d(self,newcoord,length=0.):
        self.addLength(length)
        self._coord[:2,:2]=newcoord[:]
        return newcoord[:]
    def addLength(self,length):
        self._s+=length
    def getSpos(self):
        return self._s
    def set6d(self,newcoord,length=0.):
        self.addLength(length)
        self._coord=newcoord[:]
        return newcoord[:]
    def getID(self):
        return self._ID

