# for command-line arguments
import sys
# Python Qt4 bindings for GUI objects
from PyQt4 import QtGui
# Matplotlib Figure object
from matplotlib.figure import Figure
# import the Qt4Agg FigureCanvas object, that binds Figure to
# Qt4Agg backend. It also inherits from QWidget
from matplotlib.backends.backend_qt4agg \
        import FigureCanvasQTAgg as FigureCanvas
import numpy as np

# Total number of iterations
MAXITERS = 50
# interval in millisec between updates
INTERV=300

def myFunc():
    """ Some Random Function """
    return np.random.randn(5000),np.random.randn(5000)

class MyPlot(FigureCanvas):
    """Matplotlib Figure widget to display Random Plots"""
    def __init__(self):
        # first image setup
        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)
        # initialization of the canvas
        FigureCanvas.__init__(self, self.fig)

        # set specific limits for X and Y axes
        self.ax.set_xlim(-10, 10)
        self.ax.set_ylim(-10, 10)
        # and disable figure-wide autoscale
        self.ax.set_autoscale_on(False)
        # generates first "empty" plots
        self.l_val,  =   self.ax.plot([],[],'o',label='')

        # force a redraw of the Figure
        self.fig.canvas.draw()
        # initialize the iteration counter
        self.cnt = 0
        # call the update method (to speed-up visualization)
        self.timerEvent(None)
        # start timer, trigger event every INTERV millisecs
        self.timer = self.startTimer(INTERV)

    def timerEvent(self, evt):
        """Custom timerEvent code, called at timer event receive"""

        # new data...
        x,y=myFunc()

        # update plots:
        self.l_val.set_data(x,y)
        # force a redraw of the Figure
        self.fig.canvas.draw()
        # if we've done all the iterations
        if self.cnt == MAXITERS:
            # stop the timer
            self.killTimer(self.timer)
        else:
            # else, we increment the counter
            self.cnt += 1

if __name__ == "__main__":
    # create the GUI application
    app = QtGui.QApplication(sys.argv)
    # Create our Matplotlib widget
    widget = MyPlot()
    # set the window title
    widget.setWindowTitle("Random numbers...")
    # show the widget
    widget.show()
    # start the Qt main loop execution, exiting from this script
    # with the same return code of Qt application
    sys.exit(app.exec_())



