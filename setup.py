from distutils.core import setup

setup(name='paccel',
      version='0.1',
      description='Very simple accelerator simulation package',
      author='Yngve Inntjore Levinsen',
      author_email='yngve.levinsen@esss.se',
      packages=[
          'paccel',
          'paccel.converters',
          'paccel.elements',
          ],
)
