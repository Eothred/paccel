from paccel.accelerator import accelerator

def main():
    acc=accelerator()
    acc.loadFromFile('test.yml')
    print "Name of accelerator:", acc.name
    print "Linear transfer matrix:"
    print acc.getTransferMatrix()
if __name__=="__main__":
    main()
