from paccel import accelerator
from paccel.elements.dipole import dipole,edge
from paccel.elements.quadrupole import quadrupole
from paccel.elements.drift import drift
import numpy as np

# some parameters:
ncells=5 # number of fodo cells
dl=1.5 # dipole length
dR=3.8197 # dipole bending radius
dPsi=0.1964 # edge focusing effect
qk=1.2 # quadrupole strength
qlf=.2 # f quadrupole length
qld=.4 # d quadrupole length
driftlength=.55 # drift length between each magnet

nparticles=30
nturns=10
def simulation():
    
    # Define accelerator:
    zeropos=np.zeros((2,3))
    acc=accelerator.accelerator('LHC',nparticles)
    for j in xrange(ncells):
        postfix="_"+str(j)
        acc.addElement(quadrupole('q1'+postfix,qlf,zeropos,qk))
        acc.addElement(drift('drift'+postfix,driftlength,zeropos))
        
        acc.addElement(edge('ed1pre'+postfix,zeropos,dR,dPsi))
        acc.addElement(dipole('d1'+postfix,dl,zeropos,dR))
        acc.addElement(edge('ed1post'+postfix,zeropos,dR,-dPsi))
        
        acc.addElement(drift('drift'+postfix,driftlength,zeropos))
        
        acc.addElement(quadrupole('q2'+postfix,qld,zeropos,-qk))
        
        acc.addElement(drift('drift'+postfix,driftlength,zeropos))
        
        acc.addElement(edge('ed2pre'+postfix,zeropos,dR,dPsi))
        acc.addElement(dipole('d2'+postfix,dl,zeropos,dR))
        acc.addElement(edge('ed2post'+postfix,zeropos,dR,-dPsi))
        
        acc.addElement(drift('drift'+postfix,driftlength,zeropos))
        #acc.addElement(quadrupole('q3'+postfix,qlf,zeropos,qk))
    
    print "Accelerator length:", acc.length
    
    print "Linear transfer matrix:"
    print acc.getTransferMatrix()
    ## Print table of strengths:
    #acc.printStrengthTable()
    
    # get the dictionary of our sequence:
    acc.writeSequence('test.yml')
    
    # test transport:
    acc.trackParticles(10,'tracks.txt')
    return acc
    
def plot(acc):
    import matplotlib as mpl
    from matplotlib import pyplot as P
    
    arr=np.loadtxt('tracks.txt')
    sep=[arr[arr[:,0]==j] for j in xrange(nparticles)] # split in one array per particle
    for p in sep:
        #pil=np.array([int(p[j,5]/acc.length) for j in xrange(len(p))],dtype=int)
        #turn=[p[pil == j] for j in xrange(nturns)]
        #for t in turn:
            #P.plot(t[:,5] % acc.length,t[:,1],'r-')
        P.plot(p[:,5],p[:,1],'r-')
    P.show()
def main():
    a=simulation()
    plot(a)
    return 0
if __name__=="__main__":
    main()
