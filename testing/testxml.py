from lxml import etree
import pymadx


class madmodel:
    # We should ALWAYS call this function with tags we do not manage to pick up in the xml file!
    # This way we hopefully manage to pick up on structural changes that cause problems.
    def printWarning(self,tag):
        print("WARNING, tag in model file not used: "+tag)
    def __init__(self, parser):
        self._mod=parser.getroot()
        self._opts={}
        self._seqs={}
        self._defaultoptic=''
        for child in  parser.getroot():
            if child.tag=='optics':
                for ch in child:
                    if ch.tag=='optic':
                        #print ch.attrib['name']
                        self._opts[ch.attrib['name']]=[]
                        for c in ch:
                            if c.tag=='init-files':
                                init=ch.find('init-files')
                                for cf in c:
                                    if cf.tag=='call-file':
                                        self._opts[ch.attrib['name']].append(cf.attrib)
                                    else:
                                        self.printWarning(cf.tag)
                            else:
                                self.printWarning(c.tag)
                    else:
                        self.printWarning(ch.tag)
            elif child.tag=='default-optic':
                self._defaultoptic=child.attrib['ref-name']
            elif child.tag=='sequences':
                _strlist=['particle', 'bv']
                for ch in child:
                    if ch.tag=='sequence':
                        self._seqs[ch.attrib['name']]={}
                        for c in ch:
                            if c.tag=='beam':
                                self._seqs[ch.attrib['name']]['beam']={}
                                for b in c:
                                    if b.tag in _strlist:
                                        self._seqs[ch.attrib['name']]['beam'][b.tag]=b.attrib['value']
                                    else:
                                        self._seqs[ch.attrib['name']]['beam'][b.tag]=float(b.attrib['value'])
                                        
                            elif c.tag=='ranges':
                                print 'ok'
                            elif c.tag=='default-range':
                                print 'ok'
                            else:
                                self.printWarning(c.tag)
                    else:
                        self.printWarning(ch.tag)
            else:
                self.printWarning(child.tag)
        
        
        print self._defaultoptic
        for s in self._seqs:
            print self._seqs[s]['beam']

if __name__ == "__main__":
    doc=etree.parse('lhc-lsa_jmd/lhc-lsa.jmd.xml')
    madmodel(doc)